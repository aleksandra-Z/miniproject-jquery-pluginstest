$(document).ready(function(){
    
    $( "#accordion" ).accordion({
        animate: 500, 
        collapsible: true, 
        heightStyle: "content", 
        beforeActivate: function(e, ui) {
            $(".draggable").css("width", "150px");
        }
    });
    $( "#dialog" ).dialog({ autoOpen: false });
    $( "#opener" ).click(function() {
        $( "#dialog" ).dialog( "open" );
    });
    
    var isOpen;
    var isOpenInfo;
    $("button").click(function(){
        isOpen = $( "#dialog" ).dialog( "isOpen" );
        if(isOpen)
            isOpenInfo = "Dialog box is open!";
        else
            isOpenInfo = "Dialog box is closed";
        $(".draggable").text(isOpenInfo);        
    });
    
    $(".draggable").draggable();
    $( function() {
    var availableTags = [
      "Polish",
      "English",
      "German",
      "Spanish",
      "Portuguese",
      "Swedish",
      "Greek",
      "Arabic"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
    
  } );
  
  
   $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 100,
      values: [ 10, 55 ],
      slide: function( event, ui ) {
        $( "#amount" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val("10 - 55");
      
    $( "#slider-range" ).slider({
        animate: "slow"     
    });
});